# importing flask and Mysqldb
from flask import Flask,render_template,request,redirect,session,flash,url_for
from functools import wraps
from flask_mysqldb import MySQL
#connecting flask to mysql for data modifications
app=Flask(__name__)
app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='Harshi10968@'
app.config['MYSQL_DB']='db_sample2'
app.config['MYSQL_CURSORCLASS']='DictCursor'
mysql=MySQL(app)

#Login page c reating using flask 
@app.route('/') 
@app.route('/login',methods=['POST','GET'])#using methods in flask
def login():
    status=True
    if request.method=='POST':
        name=request.form["UserName"]
        pwd=request.form["passwords"]
        cur=mysql.connection.cursor()
        cur.execute("select * from users where UserName=%s and passwords=%s",(name,pwd))
        data=cur.fetchone()
        if data:
            session['logged_in']=True
            session['username']=data["UserName"]
            flash('Login Successfully','success')
            return redirect('home')
        else:
            flash('Invalid Login. Try Again','danger')
						#using render template to return the html file
    return render_template("login.html")

#check if user logged in
def is_logged_in(f):
	@wraps(f)
	def wrap(*args,**kwargs):
		if 'logged_in' in session:
			return f(*args,**kwargs)
		else:
			flash('Unauthorized, Please Login','danger')
            #using redirect sessions to return the html page 
			return redirect(url_for('login'))
	return wrap

#Registration creation using flask and its methods
@app.route('/reg',methods=['POST','GET'])
def reg():
    status=False
    if request.method=='POST':
        name=request.form["UserName"]
        pwd=request.form["passwords"]
        conpass = request.form["confirm"]
        lan = request.form["lang"]
        fname = request.form["firstname"]
        lname = request.form["lastname"]
        nation = request.form["nationality"]
        state = request.form["state"]
        cur=mysql.connection.cursor()
        cur.execute("insert into users(UserName,passwords,confirm,lang,firstname,lastname,nationality,state) values(%s,%s,%s,%s,%s,%s,%s,%s)",(name,pwd,conpass,lan,fname,lname,nation,state,))
        mysql.connection.commit()
        cur.close()
        flash('Registration Successfully. Login Here...','success')
        return redirect('login')
    return render_template("reg.html",status=status)

#Home page creation using flask
@app.route("/home")
#using the decorator is_logged_in to check whether the user is logged in or not
@is_logged_in
def home():
	return render_template('home.html')
    
#logout using flask
@app.route("/logout")
def logout():
  	# exiting the session after logout
	session.clear()
	flash('You are now logged out','success')
	return redirect(url_for('login'))


#creating contact us page using flask
@app.route("/contactus")
def contactus():
	return render_template('contactus.html')

#train details page
@app.route("/status")
def status():
	return render_template('Trainstatus.html')

#food order page
@app.route("/order")
def order():
	return render_template('foodorder.html')

@app.route('/default/<params>')
def DEFAULT(params):
    return 'your train details are  : '+params

@app.route('/username_details/<string:name>/<int:num>')
def username_details(name,num):
    return 'User name is: '+ name + ' and user id is: ' +str(num)

@app.route('/param_login/<string:UserName>/<string:passwords>',methods=['POST','GET'])#using methods in flask
def param_login(UserName,passwords):
    # status=True
    # if request.method=='POST':
    #     name=request.args['UserName']
    #     pwd=request.args['passwords']
  cur=mysql.connection.cursor()
  cur.execute("select * from users where UserName=%s and passwords=%s",(UserName,passwords))
  data=cur.fetchone()
  if data:
      session['logged_in']=True
      session['username']=data["UserName"]
      flash('Login Successfully','success')
      return render_template('home.html')
  else:
      flash('Invalid Login. Try Again','danger')
      #using render template to return the html file
  return render_template("login.html")




if __name__=='__main__':
  	## The secret key is needed to keep the client-side sessions secure.
    app.secret_key='secret123'
    app.run(debug=True,port=50022)