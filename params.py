from flask import Flask, request
app = Flask(__name__)
@app.route('/index')
def access_param():
  name = request.args.get('name')
  return '''<h1>The source value is: {}</h1>'''.format(name)
if __name__=='__main__':
  app.run(debug=True, port=5000)